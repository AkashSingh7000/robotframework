*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/makeMyTripSearchPageLocators.robot

*** Keywords ***
Select Source Of Journey
	Click Element    ${src}
	Input Text    ${source}    ${sourceValue}
	Run Keyword And Ignore Error    Wait Until Page Contains    ${sourceElement}
	Click Element    ${sourceElement}

Select Destination Of Journey
	Click Element    ${dest}
	Input Text    ${destination}    ${destinationValue}
    Run Keyword And Ignore Error    Wait Until Page Contains    ${destinationElement}
	Click Element    ${destinationElement}

Select Date Of Journey
    Run Keyword And Ignore Error    Scroll Element Into View    ${date}
	Click Element    ${date}

Click on Search
	Click Element    ${searchButton}
    Sleep    30s
