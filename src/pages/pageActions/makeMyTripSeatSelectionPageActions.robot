*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/makeMyTripSeatSelectionPageLocators.robot

*** Keywords ***
Select Any Seat For Passenger
	@{seats}=    Get Webelements    ${seatElements}
    FOR    ${seat}    IN    @{seats}
    	${text}=    Get Text    ${seat}
    	Log To Console    ${text}
    	${status}=    Run Keyword And Return Status    Should Match    ${text}    *${zero}*
    		IF  ${status}
    		    Click Element    ${seat}
    		    Sleep    5s
    		    BREAK
    		END
    END

Click On Continue To Book Your Seat
	Click Element    ${continueToSelect}

Click On Continue Without Selecting Your Meal
	Click Element    ${continueToSelect}

Click On Proceed To Pay
	Click Element    ${proceedToPay}
	Sleep    15s