*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/makeMyTripSearchResultPageLocators.robot

*** Keywords ***
Click On Okay
	Click Element    ${okay}

Click On Non Stop Flight
	Click Element    ${nonStop}

Click On View Prices Of First Flight From Results
	Run Keyword And Ignore Error    Scroll Element Into View    ${viewPrices}
	Click Element    ${viewPrices}

Click On Book Now Of First Flight With Saver
	Scroll Element Into View    ${bookNow}
	Click Element    ${bookNow}
	Sleep    10s

Switch To The New Tab
	${newTab}=    Switch Window    NEW