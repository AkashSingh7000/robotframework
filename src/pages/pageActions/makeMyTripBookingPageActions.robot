*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/makeMyTripBookingPageLocators.robot

*** Keywords ***
Select Option To Secure Trip
	Click Element    ${secureTrip}

Click On Add New Adult To Enter Passenger Details
	Click Element    ${addNewAdult}

Enter Passenger Details
	Click Element    ${firstMiddleName}
	Input Text    ${firstMiddleName}    ${firstMiddleNameValue}
	Click Element    ${lastName}
	Input Text    ${lastName}    ${lastNameValue}
	Click Element    ${gender}
	Click Element    ${mobileNo}
	Input Text    ${mobileNo}    ${mobileNoValue}
	Click Element    ${email}
	Input Text    ${email}    ${emailValue}
	Click Element    //div[text()='${emailValue}']

Click On Confirm And Save Billing Details
	Click Element    ${confirm}

Click On Continue
	Click Element    ${continue}

Review Details And Confirm
	Click Element    ${finalConfirm}
	Sleep    15s