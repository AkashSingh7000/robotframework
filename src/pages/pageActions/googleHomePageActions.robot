*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/googleHomePageLocators.robot

*** Keywords ***
Launch Browser
	Open Browser    ${url}    ${browser}
	Maximize Browser Window

Search For MakeMyTrip
	Click Element    ${search}
	Input Text    ${search}    ${searchValue}
	Press Keys    ${search}    ENTER
	Set Selenium Implicit Wait    5

Click On Flights
	Click Element    ${flights}
	Set Selenium Implicit Wait    5
	