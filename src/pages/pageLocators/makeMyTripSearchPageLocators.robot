*** Variables ***
${src}    //input[@id="fromCity"]
${source}    //input[@type='text' and @placeholder="From"]
${sourceValue}    Kolkata
${sourceElement}    //p[text()='Kolkata, India']
${dest}    //input[@id="toCity"]
${destination}    //input[@type='text' and @placeholder="To"]
${destinationValue}     Pune
${destinationElement}    //p[text()='Pune, India']
${date}    (//p[text()='30'])[2]
${searchButton}    //a[text()='Search']