*** Variables ***
${secureTrip}     //span[contains(text(),'Secure my trip')]
${addNewAdult}    //button[text()='+ ADD NEW ADULT']
${firstMiddleName}    //input[@placeholder='First & Middle Name']
${firstMiddleNameValue}    Akash Kumar
${lastName}    //input[@placeholder='Last Name']
${lastNameValue}    Singh
${gender}    //input[@value='MALE']
${mobileNo}    //input[@placeholder='Mobile No']
${mobileNoValue}    9713895470
${email}    //input[@placeholder='Email']
${emailValue}    abc@gmail.com
${confirm}    //div[@class='checkboxWithLblWpr__checkboxCtr']
${continue}    //button[text()='Continue']
${finalConfirm}    //button[text()='CONFIRM']