*** Settings ***
Library    SeleniumLibrary
Resource    ../src/pages/pageActions/googleHomePageActions.robot
Resource    ../src/pages/pageActions/makeMyTripSearchPageActions.robot
Resource    ../src/pages/pageActions/makeMyTripSearchResultPageActions.robot
Resource    ../src/pages/pageActions/makeMyTripBookingPageActions.robot
Resource    ../src/pages/pageActions/makeMyTripSeatSelectionPageActions.robot

*** Test Cases ***
TC - Make My Trip Flight Booking
	Launch Browser
	Search For MakeMyTrip
	Click On Flights
	Select Source Of Journey
	Select Destination Of Journey
	Select Date Of Journey
	Click on Search
	Click On Okay
	Click On Non Stop Flight
	Click On View Prices Of First Flight From Results
	Click On Book Now Of First Flight With Saver
	Switch To The New Tab
	Select Option To Secure Trip
	Click On Add New Adult To Enter Passenger Details
	Enter Passenger Details
	Click On Confirm And Save Billing Details
	Click On Continue
    Review Details And Confirm
    Select Any Seat For Passenger
    Click On Continue To Book Your Seat
    Click On Continue Without Selecting Your Meal
    Click On Proceed To Pay